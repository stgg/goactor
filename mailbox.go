package goactor

import (
	"container/list"
	"sync"
)


type MailBox interface {

}

type mailBox struct {
	mu sync.Mutex
	l *list.List
}

func newMailBox() *mailBox{
	return &mailBox{
		l: list.New(),
	}
}

func (self *mailBox)push(msg interface{}) {
	self.mu.Lock()
	self.l.PushFront(msg)
	self.mu.Unlock()
}


func (self *mailBox)pick() (interface{}, bool) {
	self.mu.Lock()
	back := self.l.Back()
	if back == nil {
		self.mu.Unlock()
		return nil, false
	}

	data := self.l.Remove(back)
	self.mu.Unlock()

	return data, true
}

