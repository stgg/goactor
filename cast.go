package goactor

type castMsg struct {
	msg interface{}
}

func newCastMsg(msg interface{}) *castMsg{
	return &castMsg{msg: msg}
}
