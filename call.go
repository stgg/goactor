package goactor

import (
    "context"
    "sync/atomic"
)

type callMsg struct {
    msg interface{}         //请求的数据
    replyCh chan interface{}
    replyFlag int32
    reason error
    isInit bool             //是否是初始化消息
}

func (self *callMsg)waitCtx(ctx context.Context) (interface{}, error) {
    select {
    case v, ok := <- self.replyCh:
        if !ok {
            //目标进程关闭连接了
            return nil, self.reason
        }
        return v, nil
    case <- ctx.Done():
        return nil, ctx.Err()
    }
}

func (self *callMsg)Reply(value interface{}) {
    if atomic.CompareAndSwapInt32(&self.replyFlag, 0, 1) {
        self.replyCh <- value
    } else {
        panic("repeat")
    }
}

func (self *callMsg)Close(err error) {
    if atomic.CompareAndSwapInt32(&self.replyFlag, 0, 1) {
        self.reason = err
        close(self.replyCh)
    } else {
        panic("repeat")
    }
}

type From interface {
    Reply(value interface{})
    Close(err error)
}


func newCallMsg(msg interface{}) *callMsg {
    return &callMsg{
        msg: msg,
        replyCh: make(chan interface{}, 1),
        replyFlag: 0,
        reason: nil,
        isInit: false,
    }
}










