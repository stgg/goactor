package main

import (
	"context"
	"goactor"
	"log"
	"os"
	"reflect"
	"runtime/pprof"
	"time"
	"timingwheel"
)

type TestActor struct {
}

func (self *TestActor)Init(args interface{}, pid goactor.TINYPID) error {
	return nil
}

func (self *TestActor)HandleCall(callMsg interface{}, from goactor.From) error {
	switch v := callMsg.(type) {
	case int32:
		from.Reply(v)
	default:
		log.Fatalln(reflect.TypeOf(callMsg))
	}

	return nil
}

func (self *TestActor)HandleCast(castMsg interface{}) error {
	return nil
}

func (self *TestActor)HandleTimeout() error {
	return nil
}

func (self *TestActor)Terminate(reason error) {

}

func main() {
	t := timingwheel.NewTimingWheel(10 * time.Millisecond, 1000)
	goactor.SetTimer(t)
	pid, err := goactor.Spawn(&TestActor{}, nil, 100 * time.Millisecond)
	if err != nil {
		log.Fatalln(err)
	}
	f , err := os.Create("cpu.prof")
	if err != nil {
		log.Fatalln(err)
	}
	if err := pprof.StartCPUProfile(f); err != nil {
		log.Fatalln(err)
	}

	num := 1000000
	//chs := make([]chan struct{}, num)
	//for i:=0; i<num; i++ {
	//	chs[i] = make(chan struct{})
	//}
	t1 := time.Now()
	for i:=0; i<num; i++ {
		//go func (p goactor.PID, ch chan struct{}) {
		//	for j:=0; j<10000; j++{
				_, err := pid.Call(int64(1), context.Background())
				if err != nil {
					log.Fatalln(err)
				}
				//<- t.After(10 * time.Millisecond)
			//}
			//ch <- struct{}{}
		//}(pid, chs[i])
	}
	//
	//for i:=0; i<num; i++ {
	//	<- chs[i]
	//}
	v := time.Since(t1)
	log.Printf("%v %v", v, v / time.Duration(num))
	pprof.StopCPUProfile()



//	c := make(chan os.Signal,1)
//	signal.Notify(c, syscall.SIGINT)
//LOOP:
//	for {
//		select {
//		case v := <- c:
//			log.Printf("%s", v)
//			break LOOP
//		}
//	}
}