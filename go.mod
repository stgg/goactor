module goactor

go 1.13

replace timingwheel v0.0.0 => ./timingwheel

require timingwheel v0.0.0
