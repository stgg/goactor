package goactor

import (
	"context"
	"reflect"
	"sync"
	"sync/atomic"
	"time"
)

type Actor interface {
	Init(interface{}, TINYPID)  error
	HandleCall(callMsg interface{}, from From) error
	HandleCast(castMsg interface{}) error
	HandleTimeout() error
	Terminate(reason error)
}

func Spawn(mod Actor, args interface{}, timeout time.Duration)(PID, error) {
	p := &process{
		actor: mod,
		closeCh: make(chan struct{}),
		closeFlag: 0,

		timeout: timeout,

		//callCh:  make(chan *callMsg),
		sigCh: make(chan struct{}, 1),
		mailBox: newMailBox(),
	}
	p.WaitGroup.Add(1)
	go p.loop()

	err := p.init(args)
	if err != nil {
		return nil, err
	}
	//执行初始化操作
	//ctx, err := mod.Init(args)
	//if err != nil {
	//	//进程退出
	//	close(p.closeCh)
	//	return nil, err
	//}
	//p.ctx = ctx
	return &pid{p: p}, nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type process struct {
	actor Actor

	closeCh chan struct{}
	closeFlag int32

	timeout time.Duration
	//callCh chan *callMsg
	sigCh chan struct{}
	mailBox *mailBox					//存放cast的消息

	sync.WaitGroup
}




func (self *process)loop() {
	defer func() {
		self.WaitGroup.Done()
	}()

	selectCase := make([]reflect.SelectCase, 0, 3)
	selectCase = append(selectCase, reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(self.closeCh)})
	selectCase = append(selectCase, reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(self.sigCh)})
	isSet := false
	var timer <- chan struct{}
	for {
		selectCase = selectCase[:2]
		if self.timeout > 0 {
			if !isSet {
				isSet = true
				timer = actorTimer.After(self.timeout)
			}
			selectCase = append(selectCase, reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(timer)})
		}

		chosen, _, _ := reflect.Select(selectCase)

		switch chosen {
		case 0: //closeCh
			return
		case 1: //sigCh
			msg, ok := self.mailBox.pick()
			if ok {
				switch v := msg.(type) {
				case *castMsg:
					if err := self.actor.HandleCast(v.msg); err != nil {
						self.stop(err)
						return
					}
				case *callMsg:
					if v.isInit {
						//初始化消息
						if err := self.actor.Init(v.msg, &pid{p: self}); err != nil {
							//初始化失败了，直接返回
							v.Close(err)
							//close(self.closeCh)
							return		//退出循环
						} else {
							v.Reply(struct {}{})
						}
					} else {
						if err := self.actor.HandleCall(v.msg, v); err != nil {
							self.stop(err)
							return
						}
					}
				default:
					panic("unknown message")
				}

				//进程里可能还有其他的cast信息
				self.emit()
			}
		case 2:		//timerCh
			isSet = false
			if err := self.actor.HandleTimeout(); err != nil {
				self.stop(err)
				return
			}

		}
	}
}

func (self *process)stop(reason error) {
	if atomic.CompareAndSwapInt32(&self.closeFlag, 0, 1) {
		close(self.closeCh)
		go func() {
			self.WaitGroup.Wait()
			self.actor.Terminate(reason)
		}()
	}
}


func (self *process)init(msg interface{}) error {
	callMsg := newCallMsg(msg)
	callMsg.isInit = true
	self.mailBox.push(callMsg)

	self.emit()

	select {
	case _, ok := <- callMsg.replyCh:
		if !ok {
			//目标进程关闭通道了
			return callMsg.reason
		}
		return nil
	}
}


func (self *process)call(msg interface{}, ctx context.Context)(interface{}, error) {
	select {
	case <- self.closeCh:
		return nil, TargetClosed
	default:
	}
	callMsg := newCallMsg(msg)
	self.mailBox.push(callMsg)

	self.emit()

	select {
	case <- self.closeCh:
		return nil, TargetClosed
	case <- ctx.Done():
		return nil, ctx.Err()
	case v, ok := <- callMsg.replyCh:
		if !ok {
			//目标进程关闭通道了
			return nil, callMsg.reason
		}
		return v, nil
	}
}

func (self *process)cast(msg interface{}) {
	select {
	case <- self.closeCh:
		return
	default:
	}

	//消息放入进程邮箱
	castMsg := newCastMsg(msg)
	self.mailBox.push(castMsg)

	self.emit()
}


func (self *process)emit() {
	select {
	case self.sigCh <- struct{}{}:
	default:
	}
}
