package goactor

import "time"

type Timer interface {
	After(timeout time.Duration) <-chan struct{}
}

var actorTimer Timer



func SetTimer(t Timer) {
	actorTimer = t
}