package goactor

import (
	"context"
	"errors"
	"testing"
	"time"
	//"bitbucket.org/stgg/goactor/timingwheel"
)


type CallActor struct {
}

func (self *CallActor)Init(args interface{}, pid TINYPID) error {
	return nil
}

func (self *CallActor)HandleCall(callMsg interface{}, from From) error {
	//log.Printf("%s", callMsg.(string))
	msg := callMsg.(string)
	if msg == "call req" {
		from.Reply("call rsp")
		return nil
	} else if msg == "error test req" {
		from.Close(errors.New("error test rsp"))
		return nil
	} else if msg == "exit" {
		from.Reply("ok")
		return errors.New("initiative exit")
	} else {
		from.Close(errors.New("Unprocessable request"))
		return nil
	}
}

func (self *CallActor)HandleCast(castMsg interface{}) error {
	return nil
}

func (self *CallActor)HandleTimeout() error {
	return nil
}

func (self *CallActor)Terminate(reason error) {

}

func TestProcess(t *testing.T) {
	//timingwheel.InitTimingWheel(10 * time.Millisecond, 100000)
	pid ,err := Spawn(&CallActor{}, t, 100 * time.Millisecond)
	if err != nil {
		t.Fatalf("error1, %v", err)
	}

	callRsp, err := pid.Call("call req", context.Background())
	if err != nil {
		t.Fatalf("error2, %v", err)
	}
	if callRsp.(string) != "call rsp" {
		t.Fatalf("error3")
	}

	callRsp, err = pid.Call("error test req", context.Background())
	if callRsp != nil {
		t.Fatalf("error4")
	}

	if err == nil || err.Error() != "error test rsp"{
		t.Fatalf("error5")
	}

	time.Sleep(1 * time.Second)

	callRsp, err = pid.Call("exit", context.Background())
	if err != nil {
		t.Fatalf("error6, %v", err)
	}

	if callRsp.(string) != "ok" {
		t.Fatalf("error7, %v", err)
	}

	_, err = pid.Call("exit test", context.Background())
	if err != Err1 {
		t.Fatalf("error8, %v", err)
	}

}

//startTime := time.Now()
//pCount := 200
//loopCount := 50000
//chs := make([]chan struct{}, 0, pCount)
//for i:=0; i<pCount; i++ {
//ch := make(chan struct{})
//chs = append(chs, ch)
//go func(c chan struct{}) {
//for j:=0; j<loopCount; j++ {
//result, err := pid.Call("call hello world", context.Background())
//if err != nil {
//log.Fatalln(err)
//}
//
//intValue := result.(int32)
//if intValue != 1 {
//t.Fatalf("call reply value error, %v", intValue)
//}
//}
//
//c <- struct{}{}
//}(ch)
//}
//for i:=0; i<pCount; i++ {
//<- chs[i]
//}
//
//pid.Stop(errors.New("testing"))
//elapsed := time.Since(startTime)
//t.Logf("elapsed:%v", elapsed)