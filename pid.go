package goactor

import "context"

type PID interface {
	Call(callMsg interface{}, ctx context.Context)(interface{}, error)
	Cast(castMsg interface{})
	Stop(reason error)
}

type TINYPID interface {
	Cast(castMsg interface{})
}


type pid struct {
	p *process
}



func (self *pid)Call(msg interface{}, ctx context.Context)(interface{}, error) {
	return self.p.call(msg, ctx)
}

func (self *pid)Cast(msg interface{}) {
	self.p.cast(msg)
}

func (self *pid)Stop(reason error) {
	self.p.stop(reason)
}

